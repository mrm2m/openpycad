import sys

class Manipulation:
    def __init__(self, vec):
        self.vec = vec
        self.name = ""

    def __str__(self):
        return "%s(%r)" % (type(self).__name__.lower(), self.vec)


class Rotate(Manipulation):
    pass


class Translate(Manipulation):
    pass


class Shape():
    def __init__(self):
        self.manipulation_stack = []

    def translate(self, vec):
        self.manipulation_stack.append(Translate(vec))
        return self

    def rotate(self, vec):
        self.manipulation_stack.append(Rotate(vec))
        return self

    def __str__(self):
        if self.manipulation_stack:
            return "%s %%s" % (" ".join([str(x) for x in self.manipulation_stack[::-1]]))
        else:
            return "%%s" % ()

    def __add__(self, other):
        u = Union(self)
        u.append(other)
        return u

    def __sub__(self, other):
        return Difference(self, other)

    def center_to(self, other):
        for i in other.manipulation_stack:
            if isinstance(i, Rotate):
                self.rotate([-x for x in i.vec])
        self.manipulation_stack += other.manipulation_stack
        return self


class Union(Shape):
    def __init__(self, root_shape):
        super().__init__()
        self.shapes = [root_shape]

    def append(self, other):
        self.shapes.append(other)

    def __add__(self, other):
        self.append(other)
        return self

    def __str__(self):
        result = "union() {\n"
        result += "\n".join([str(shape) for shape in self.shapes])
        result += "\n}"
        return super().__str__() % (result)


class Difference(Shape):
    def __init__(self, minuend, subtrahend):
        super().__init__()
        self.minuend = minuend
        self.subtrahend = subtrahend

    def __str__(self):
        return super().__str__() % "difference() {\n%s\n%s\n}" % (self.minuend, self.subtrahend)


class Sphere(Shape):
    def __init__(self, diameter, fn=100):
        super().__init__()
        self.diameter = diameter
        self.fn = fn

    def __str__(self):
        return super().__str__() % ("sphere(d=%d, $fn=%d);" % (self.diameter, self.fn), )

class Cylinder(Shape):
    def __init__(self, height, diameter, fn=100):
        super().__init__()
        self.diameter = diameter
        self.height = height
        self.fn = fn

    def __str__(self):
        return super().__str__() % ("cylinder(%d, d=%d, center=true, $fn=%s);" % 
                (self.height, self.diameter, self.fn), )


class Cube(Shape):
    def __init__(self, vec):
        super().__init__()
        self.vec = vec

    def __str__(self):
        return super().__str__() % ("cube(%r, center=true);" % self.vec)



if __name__ == "__main__":
    s = Sphere(10).rotate([90,0,0]).translate([0,0,50])
    s -= Cylinder(10, 5).center_to(s).translate([0,0,5])
    c = Cylinder(10, 5).translate([0,0,10])
    d = Cube([10,10,10]).rotate([45, 0, 0])
    e = c + d
    e += (c - d).translate([20,0,0])
    e += (d - c).translate([-20,0,0])
    if(len(sys.argv) == 1):
        print(str(s))
        print(str(e))
    else:
        with open(sys.argv[1], "w") as fd:
            fd.write(str(s))
            fd.write(str(e))
